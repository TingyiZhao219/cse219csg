
{
    "TaData":{
        "startHour":"9",
        "endHour":"20",
        "undergrad_tas":[
        ],
        "officeHours":[
        ]
    },
    "CourseData":{
        "Course_num":"219",
        "Course_semester":"Spring",
        "Course_subject":"CSE",
        "Course_title":"Computer Science III",
        "Course_year":"2017",
        "Export_dir":"..\\..\\NewWeb",
        "Instructor_home":"richard@cs.stonybrook.edu",
        "Instructor_name":"Richard McKenna",
        "Left_image":"380",
        "Right_image":"381",
        "School_image":"214",
        "Site_template":"114"
    }
}