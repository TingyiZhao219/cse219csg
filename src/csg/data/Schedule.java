/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import java.time.LocalDate;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author zhaotingyi
 */
public class Schedule <E extends Comparable<E>> implements Comparable<E>{
    private StringProperty type;
    private LocalDate date;
    private StringProperty time;
    private StringProperty title;
    private StringProperty topic;
    private StringProperty link;
    private StringProperty criteria;
    
    public Schedule(String type, LocalDate date, String time, String title, String topic, String link, String criteria){
        this.type = new SimpleStringProperty(type);
        this.date = date;
        this.time = new SimpleStringProperty(time);
        this.title = new SimpleStringProperty(title);
        this.topic = new SimpleStringProperty(topic);
        this.link = new SimpleStringProperty(link);
        this.criteria = new SimpleStringProperty(criteria);
    }
    
    public String getType(){
        return type.get();
    }
    
    public LocalDate getDate(){
        return date;
    }
    
    public String getTime(){
        return time.get();
    }
    
    public String getTitle(){
        return title.get();
    }
    
    public String getTopic(){
        return topic.get();
    }
    
    public String getLink(){
        return link.get();
    }
    
    public String getCriteria(){
        return criteria.get();
    }

    @Override
    public int compareTo(E otherTA) {
        if(getDate().compareTo(((Schedule)otherTA).getDate()) == 0)
            return getTime().compareTo(((Schedule)otherTA).getTime());
        else
            return getDate().compareTo(((Schedule)otherTA).getDate());
    }
}
