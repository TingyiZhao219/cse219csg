/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;

/**
 *
 * @author zhaotingyi
 */
public class Team <E extends Comparable<E>> implements Comparable<E>{
    private StringProperty name;
    private Color color;
    private Color text_color;
    private StringProperty link;
    
    public Team(String name, Color color, Color text_color, String link){
        this.name = new SimpleStringProperty(name);
        this.color = color;
        this.text_color = text_color;
        this.link = new SimpleStringProperty(link);
    }

    public String getName() {
        return name.get();
    }

    public Color getColor() {
        return color;
    }

    public Color getText_color() {
        return text_color;
    }

    public String getLink() {
        return link.get();
    }
    
    @Override
    public int compareTo(E otherTA) {
        return getName().compareTo(((Team)otherTA).getName());
    }
    
    @Override
    public String toString(){
        return name.get();
    }
}
