/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author zhaotingyi
 */
public class Student <E extends Comparable<E>> implements Comparable<E>{
    private StringProperty fname;
    private StringProperty lname;
    private StringProperty team;
    private StringProperty role;
    
    public Student(String fname, String lname, String team, String role){
        this.fname = new SimpleStringProperty(fname);
        this.lname = new SimpleStringProperty(lname);
        this.team = new SimpleStringProperty(team);
        this.role = new SimpleStringProperty(role);
    }

    public String getFname() {
        return fname.get();
    }

    public String getLname() {
        return lname.get();
    }

    public String getTeam() {
        return team.get();
    }

    public String getRole() {
        return role.get();
    }

    @Override
    public int compareTo(E otherTA) {
        if(getFname().compareTo(((Student)otherTA).getFname()) == 0)
            return getLname().compareTo(((Student)otherTA).getLname());
        return getFname().compareTo(((Student)otherTA).getFname());
    }
}
