/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;

/**
 *
 * @author zhaotingyi
 */
public class CsgCourseData {
    
    private final CourseSiteGenerator app;
    private String course_subject;
    private String course_num;
    private String course_semester;
    private String course_year;
    private String course_title;
    private String instructor_name;
    private String instructor_home;
    private String export_dir;
    private String site_template;
    
    private String school_image;
    private String left_image;
    private String right_image;
    
    public static final String COURSE_SUBJECT = "CSE";
    public static final String COURSE_NUM = "219";
    public static final String COURSE_SEMESTER = "Spring";
    public static final String COURSE_YEAR = "2017";
    public static final String COURSE_TITLE = "Computer Science III";
    public static final String INSTRUCTOR_NAME = "Richard McKenna";
    public static final String INSTRUCTOR_HOME = "richard@cs.stonybrook.edu";
    public static final String EXPORT_DIR = "..\\..\\NewWeb";
    public static final String SITE_TEMPLATE = "114";
    public static final String SCHOOL_IMAGE = ".\\public_html\\images\\SBUDarkRedShieldLogo.png";
    public static final String LEFT_IMAGE = "380";
    public static final String RIGHT_IMAGE = "381";
    
    public CsgCourseData(CourseSiteGenerator initApp){
        app = initApp;
        course_subject = COURSE_SUBJECT;
        course_num = COURSE_NUM;
        course_semester = COURSE_SEMESTER;
        course_year = COURSE_YEAR;
        course_title = COURSE_TITLE;
        instructor_name = INSTRUCTOR_NAME;
        instructor_home = INSTRUCTOR_HOME;
        export_dir = EXPORT_DIR;
        site_template = SITE_TEMPLATE;
        school_image = SCHOOL_IMAGE;
        left_image = LEFT_IMAGE;
        right_image = RIGHT_IMAGE;
    }
    
    public void resetData(){
        course_subject = COURSE_SUBJECT;
        course_num = COURSE_NUM;
        course_semester = COURSE_SEMESTER;
        course_year = COURSE_YEAR;
        course_title = COURSE_TITLE;
        instructor_name = INSTRUCTOR_NAME;
        instructor_home = INSTRUCTOR_HOME;
        export_dir = EXPORT_DIR;
        site_template = SITE_TEMPLATE;
        school_image = SCHOOL_IMAGE;
        left_image = LEFT_IMAGE;
        right_image = RIGHT_IMAGE;
    }
    
    
    
    /////

    public String getCourse_subject() {
        return course_subject;
    }

    public void setCourse_subject(String course_subject) {
        this.course_subject = course_subject;
    }

    public String getCourse_num() {
        return course_num;
    }

    public void setCourse_num(String course_num) {
        this.course_num = course_num;
    }

    public String getCourse_semester() {
        return course_semester;
    }

    public void setCourse_semester(String course_semester) {
        this.course_semester = course_semester;
    }

    public String getCourse_year() {
        return course_year;
    }

    public void setCourse_year(String course_year) {
        this.course_year = course_year;
    }

    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getInstructor_name() {
        return instructor_name;
    }

    public void setInstructor_name(String instructor_name) {
        this.instructor_name = instructor_name;
    }

    public String getInstructor_home() {
        return instructor_home;
    }

    public void setInstructor_home(String instructor_home) {
        this.instructor_home = instructor_home;
    }

    public String getExport_dir() {
        return export_dir;
    }

    public void setExport_dir(String export_dir) {
        this.export_dir = export_dir;
    }

    public String getSite_template() {
        return site_template;
    }

    public void setSite_template(String site_template) {
        this.site_template = site_template;
    }

    public String getSchool_image() {
        return school_image;
    }

    public void setSchool_image(String school_image) {
        this.school_image = school_image;
    }

    public String getLeft_image() {
        return left_image;
    }

    public void setLeft_image(String left_image) {
        this.left_image = left_image;
    }

    public String getRight_image() {
        return right_image;
    }

    public void setRight_image(String right_image) {
        this.right_image = right_image;
    }
    
}
