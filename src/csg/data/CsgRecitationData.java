/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author zhaotingyi
 */
public class CsgRecitationData {
    private final CourseSiteGenerator app;
    ObservableList<Recitation> recitations;
    
    public CsgRecitationData(CourseSiteGenerator app){
        this.app = app;
        recitations = FXCollections.observableArrayList();
    }
    
    public void resetData(){
        recitations.clear();
    }
    
    public ObservableList getRecitations() {
        return recitations;
    }
    
    public Recitation getRecitation(String section) {
        for (Recitation rc : recitations) {
            if (rc.getSection().equals(section)) {
                return rc;
            }
        }
        return null;
    }
    
    public boolean containsRecitation(String section) {
        for (Recitation ta : recitations) {
            if (ta.getSection().equals(section)) {
                return true;
            }
        }
        return false;
    }
    
    public void addRecitation(String section, String instructor, String time, String location, String TA1, String TA2) {
        Recitation rc = new Recitation(section, instructor, time, location, TA1, TA2);
        if (!containsRecitation(section)) {
            recitations.add(rc);
        }
        Collections.sort(recitations);
    }
    
    
    public void removeRecitation(String section) {
        for (Recitation rc : recitations) {
            if (section.equals(rc.getSection())) {
                recitations.remove(rc);
                return;
            }
        }
    }
    
}
