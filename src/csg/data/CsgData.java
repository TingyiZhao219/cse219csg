/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;

/**
 *
 * @author zhaotingyi
 */
public class CsgData {
    private final CourseSiteGenerator app;
    private final CsgTAData TAData;
    private final CsgCourseData CourseData;
    private final CsgRecitationData RecitationData;
    private final CsgSchedulesData ScheduleData;
    private final CsgTeamData TeamData;
    private final CsgStudentData StudentData;
    
    public CsgData(CourseSiteGenerator initapp){
        app = initapp;
        TAData = new CsgTAData(initapp);
        CourseData = new CsgCourseData(initapp);
        RecitationData = new CsgRecitationData(initapp);
        ScheduleData = new CsgSchedulesData(initapp);
        TeamData = new CsgTeamData(initapp);
        StudentData = new CsgStudentData(initapp);
    }
    
    public CsgTAData getTAData(){
        return TAData;
    }

    public CsgCourseData getCourseData() {
        return CourseData;
    }

    public CsgRecitationData getRecitationData() {
        return RecitationData;
    }

    public CsgSchedulesData getScheduleData() {
        return ScheduleData;
    }

    public CsgTeamData getTeamData() {
        return TeamData;
    }

    public CsgStudentData getStudentData() {
        return StudentData;
    }
    
    public void resetData(){
        TAData.resetData();
        CourseData.resetData();
        RecitationData.resetData();
        ScheduleData.resetData();
        TeamData.resetData();
        StudentData.resetData();
    }
    
}
