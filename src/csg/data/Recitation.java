/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author zhaotingyi
 */
public class Recitation <E extends Comparable<E>> implements Comparable<E>{
    private StringProperty section;
    private StringProperty instructor;
    private StringProperty time;
    private StringProperty location;
    private StringProperty TA1;
    private StringProperty TA2;
    
    public Recitation(String section, String instructor, String time, String location, String TA1, String TA2){
        this.section = new SimpleStringProperty(section);
        this.instructor = new SimpleStringProperty(instructor);
        this.time = new SimpleStringProperty(time);
        this.location = new SimpleStringProperty(location);
        this.TA1 = new SimpleStringProperty(TA1);
        this.TA2 = new SimpleStringProperty(TA2);
    }
    
    public String getSection(){
        return section.get();
    }
    
    public String getInstructor(){
        return instructor.get();
    }
    
    public String getTime(){
        return time.get();
    }
    
    public String getLocation(){
        return location.get();
    }
    
    public String getTA1(){
        return TA1.get();
    }
    
    public String getTA2(){
        return TA2.get();
    }

    @Override
    public int compareTo(E otherTA) {
        return getSection().compareTo(((Recitation)otherTA).getSection());
    }
}
