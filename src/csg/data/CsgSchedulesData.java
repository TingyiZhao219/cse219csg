/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;
import java.time.LocalDate;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author zhaotingyi
 */
public class CsgSchedulesData {
    private final CourseSiteGenerator app;
    ObservableList<Schedule> schedules;
    private LocalDate start;
    private LocalDate end;
    
    public CsgSchedulesData(CourseSiteGenerator app){
        this.app = app;
        schedules = FXCollections.observableArrayList();
        start = LocalDate.now();
        end = LocalDate.now();
    }
    
    public void resetData(){
        schedules.clear();
        start = LocalDate.now();
        end = LocalDate.now();
    }
    
    public ObservableList getSchedules() {
        return schedules;
    }
    
    public Schedule getSchedule(LocalDate date) {
        for (Schedule sc : schedules) {
            if (sc.getDate().equals(date)) {
                return sc;
            }
        }
        return null;
    }
    
    public boolean containsSchedule(LocalDate date) {
        for (Schedule sc : schedules) {
            if (sc.getDate().equals(date)) {
                return true;
            }
        }
        return false;
    }
    
    public void addSchedule(String type, LocalDate date, String time, String title, String topic, String link, String criteria) {
        Schedule sc = new Schedule(type, date, time, title, topic, link, criteria);
        if (!containsSchedule(date)) {
            schedules.add(sc);
        }
        Collections.sort(schedules);
    }
    
    
    public void removeSchedule(LocalDate date) {
        for (Schedule sc : schedules) {
            if (sc.getDate().equals(date)) {
                schedules.remove(sc);
                return;
            }
        }
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }
    
    
}
