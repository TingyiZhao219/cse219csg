/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

/**
 *
 * @author zhaotingyi
 */
public class CsgTeamData {
    private final CourseSiteGenerator app;
    ObservableList<Team> teams;
    
    public CsgTeamData(CourseSiteGenerator app){
        this.app = app;
        teams = FXCollections.observableArrayList();
    }
    
    public void resetData(){
        teams.clear();
    }
    
    public ObservableList getTeams() {
        return teams;
    }
    
    public Team getTeam(String name) {
        for (Team te : teams) {
            if (te.getName().equals(name)) {
                return te;
            }
        }
        return null;
    }
    
    public boolean containsTeam(String name) {
        for (Team te : teams) {
            if (te.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }
    
    public void addTeam(String name, Color color, Color text_color, String link) {
        Team te = new Team(name, color, text_color, link);
        if (!containsTeam(name)) {
            teams.add(te);
        }
        Collections.sort(teams);
    }
    
    
    public void removeTeam(String name) {
        for (Team te : teams) {
            if (name.equals(te.getName())) {
                teams.remove(te);
                return;
            }
        }
    }
    
}
