/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.data;

import csg.CourseSiteGenerator;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;

/**
 *
 * @author zhaotingyi
 */
public class CsgStudentData {
    private final CourseSiteGenerator app;
    ObservableList<Student> students;
    
    public CsgStudentData(CourseSiteGenerator app){
        this.app = app;
        students = FXCollections.observableArrayList();
    }
    
    public void resetData(){
        students.clear();
    }
    
    public ObservableList getStudents() {
        return students;
    }
    
    public Student getStudent(String fname, String lname) {
        for (Student st : students) {
            if (st.getFname().equals(fname) && st.getLname().equals(lname)) {
                return st;
            }
        }
        return null;
    }
    
    public boolean containsStudent(String fname, String lname) {
        for (Student st : students) {
            if (st.getFname().equals(fname) && st.getLname().equals(lname)) {
                return true;
            }
        }
        return false;
    }
    
    public void addStudent(String fname, String lname, String team, String role) {
        Student st = new Student(fname, lname, team, role);
        if (!containsStudent(fname, lname)) {
            students.add(st);
        }
        Collections.sort(students);
    }
    
    
    public void removeStudent(String fname, String lname) {
        for (Student st : students) {
            if (st.getFname().equals(fname) && st.getLname().equals(lname)) {
                students.remove(st);
                return;
            }
        }
    }
    
}
