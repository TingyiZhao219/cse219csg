/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg;

import csg.data.CsgData;
import csg.file.CsgFile;
import csg.file.CsgFileController;
import csg.style.CsgStyle;
import csg.ui.AppMessageDialogSingleton;
import csg.ui.AppYesNoCancelDialogSingleton;
import csg.workspace.CsgWorkspace;
import java.util.Optional;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CourseSiteGenerator extends Application {
    private static CsgWorkspace workspaceComponent;
    private static CsgData dataComponent;
    private static CsgStyle styleComponent;
    private static CsgFileController fileController;
    private static CsgFile fileComponent;
    private static Stage initStage;
    private static Scene initScene;
    
    
    @Override
    public void start(Stage stage) throws Exception {
        initStage = stage;
        AppMessageDialogSingleton messageDialog = AppMessageDialogSingleton.getSingleton();
	messageDialog.init(initStage);
	AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
	yesNoDialog.init(initStage);
        
//	try {
//	    // LOAD APP PROPERTIES, BOTH THE BASIC UI STUFF FOR THE FRAMEWORK
//	    // AND THE CUSTOM UI STUFF FOR THE WORKSPACEAlert _alert = new Alert(Alert.AlertType.CONFIRMATION,p_message,new ButtonType("取消", ButtonBar.ButtonData.NO),
//               Alert _alert = new Alert(Alert.AlertType.CONFIRMATION,"",new ButtonType("English", ButtonBar.ButtonData.NO),new ButtonType("Chinese", ButtonBar.ButtonData.YES));
//                _alert.setTitle("Choose your language");
//                _alert.setHeaderText("Choose your language.");
//                Optional<ButtonType> _buttonType = _alert.showAndWait();
//                if(_buttonType.get().getButtonData().equals(ButtonBar.ButtonData.YES)){
//                    PropertiesManager props = PropertiesManager.getPropertiesManager();
//                    props.addProperty("DATA_PATH", "./data/");
//                    props.loadProperties("app_properties_chinese.xml", "properties_schema.xsd");
//                }
//                else {
//                    PropertiesManager props = PropertiesManager.getPropertiesManager();
//                    props.addProperty("DATA_PATH", "./data/");
//                    props.loadProperties("app_properties_english.xml", "properties_schema.xsd");
//                }
//	}catch (Exception e) {
//	}
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        props.addProperty("DATA_PATH", "./data/");
        props.loadProperties("app_properties_english.xml", "properties_schema.xsd");
//        workspaceComponent = new CourseSiteGeneratorWorkspace(this);
        dataComponent = new CsgData(this);
        styleComponent = new CsgStyle(this);
        fileComponent = new CsgFile(this);
        
        Parent root = FXMLLoader.load(getClass().getResource("csg.fxml"));
        
        initScene = new Scene(root);
        
        stage.setScene(initScene);
        stage.show();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static CsgWorkspace getWorkspace(){
        return workspaceComponent;
    }
    
    public static void setWorkspace(CsgWorkspace initWorkspace){
        workspaceComponent = initWorkspace;
    }
    
    public static CsgData getData(){
        return dataComponent;
    }

    public static void setData(CsgData dataComponent) {
        CourseSiteGenerator.dataComponent = dataComponent;
    }

    public static CsgStyle getStyle() {
        return styleComponent;
    }

    public static void setStyle(CsgStyle styleComponent) {
        CourseSiteGenerator.styleComponent = styleComponent;
    }

    public static CsgFileController getFileController() {
        return fileController;
    }

    public static void setFileController(CsgFileController fileController) {
        CourseSiteGenerator.fileController = fileController;
    }

    public static Stage getInitStage() {
        return initStage;
    }

    public static Scene getInitScene() {
        return initScene;
    }

    public static CsgFile getFileComponent() {
        return fileComponent;
    }

    public static void setFileComponent(CsgFile fileComponent) {
        CourseSiteGenerator.fileComponent = fileComponent;
    }

    
    
    
    
}
