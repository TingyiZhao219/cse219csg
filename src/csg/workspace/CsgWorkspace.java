/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGenerator;
import csg.CourseSiteGeneratorProp;
import csg.data.CsgCourseData;
import csg.data.CsgData;
import csg.data.CsgRecitationData;
import csg.data.CsgSchedulesData;
import csg.data.CsgStudentData;
import csg.data.CsgTAData;
import csg.data.CsgTeamData;
import csg.data.Recitation;
import csg.data.Schedule;
import csg.data.Student;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.file.CsgFileController;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CsgWorkspace implements Initializable{
    
//    private final CourseSiteGenerator app;
    
//    public CourseSiteGeneratorWorkspace(CourseSiteGenerator initapp){
//        app = initapp;
        
//    }
    @FXML private AnchorPane MainPane;
    @FXML private Menu menu_file;
    @FXML private Menu menu_edit;
    @FXML private Menu menu_help;
    @FXML private MenuItem menu_file_new;
    @FXML private MenuItem menu_file_save;
    @FXML private MenuItem menu_file_saveAs;
    @FXML private MenuItem menu_file_load;
    @FXML private MenuItem menu_file_export;
    @FXML private MenuItem menu_file_exit;
    @FXML private MenuItem menu_edit_undo;
    @FXML private MenuItem menu_edit_redo;
    @FXML private MenuItem menu_help_about;
    //
    @FXML private Tab tab_courseDetails;
    @FXML private Tab tab_taData;
    @FXML private Tab tab_recitationData;
    @FXML private Tab tab_scheduleData;
    @FXML private Tab tab_projectData;
    //
    //Things in Course Tab
//    @FXML private Label course_title;
    @FXML private ComboBox course_subject;
    @FXML private ComboBox course_number;
    @FXML private ComboBox course_semester;
    @FXML private ComboBox course_year;
    @FXML private TextField course_title;
    @FXML private TextField course_instru_name;
    @FXML private TextField course_instru_home;
    @FXML private Button export_dir_button;
    @FXML private TableView course_site_table;
//    @FXML private TableColumn<CourseSites, Boolean> course_use_column;
    @FXML private TableColumn<CourseSites, CheckBox> course_use_column;
    @FXML private TableColumn<CourseSites, String> course_title_column;
    @FXML private TableColumn<CourseSites, String> course_file_column;
    @FXML private TableColumn<CourseSites, String> course_script_column;
    @FXML private ImageView course_school_image;
    @FXML private Button course_school_button;
    @FXML private ImageView course_left_image;
    @FXML private Button course_left_button;
    @FXML private ImageView course_right_image;
    @FXML private Button course_right_button;
    @FXML private ComboBox course_stylesheet;
    @FXML private Label export_dir;
    @FXML private Label course_template_dir;
    @FXML private Button course_template_button;
    //Things in TA Tab
    @FXML private Text TA_taTitle;
    @FXML private TableView<TeachingAssistant> TA_table;
    @FXML private TableColumn<TeachingAssistant, CheckBox> TAundergradColumn;
    @FXML private TableColumn<TeachingAssistant, String> TAnameColumn; 
    @FXML private TableColumn<TeachingAssistant, String> TAemailColumn;
    @FXML private TextField TAnameinput;
    @FXML private TextField TAemailinput;
    @FXML private Button TAaddButton;
    @FXML private Button TAclearButton;
    @FXML private Text OfficeHourTitle;
    @FXML private Text OfficeHourStart;
    @FXML private Text OfficeHourEnd;
    @FXML private ComboBox OfficeHourStartBox;
    @FXML private ComboBox OfficeHourEndBox;
    ObservableList<String> time_options;
    @FXML private GridPane OfficeHoursGridPane;
    /////Recitation tab things
    @FXML private TableView recitation_table;
    @FXML private TableColumn<Recitation, String> recitation_section_column;
    @FXML private TableColumn<Recitation, String> recitation_instructor_column;
    @FXML private TableColumn<Recitation, String> recitation_time_column;
    @FXML private TableColumn<Recitation, String> recitation_location_column;
    @FXML private TableColumn<Recitation, String> recitation_TA1_column;
    @FXML private TableColumn<Recitation, String> recitation_TA2_column;
    @FXML private TextField recitation_section;
    @FXML private TextField recitation_instructor;
    @FXML private TextField recitation_time;
    @FXML private TextField recitation_location;
    @FXML private ComboBox recitation_TA1;
    @FXML private ComboBox recitation_TA2;
    @FXML private Button recitation_add;
    @FXML private Button recitation_clear;
    @FXML private Button recitation_delet;
    ///schedule tab things
    @FXML private DatePicker schedule_start;
    @FXML private DatePicker schedule_end;
    @FXML private TableView schedule_table;
    @FXML private TableColumn<Schedule, String> schedule_type_column;
    @FXML private TableColumn<Schedule, String> schedule_date_column;
    @FXML private TableColumn<Schedule, String> schedule_title_column;
    @FXML private TableColumn<Schedule, String> schedule_topic_column;
    @FXML private ComboBox schedule_type;
    @FXML private DatePicker schedule_date;
    @FXML private TextField schedule_time;
    @FXML private TextField schedule_title;
    @FXML private TextField schedule_topic;
    @FXML private TextField schedule_link;
    @FXML private TextField schedule_criteria;
    @FXML private Button schedule_add;
    @FXML private Button schedule_clear;
    @FXML private Button schedule_delet;
    ///project tab teams
    @FXML private TableView team_table;
    @FXML private TableColumn<Team, String> team_name_column;
    @FXML private TableColumn<Team, String> team_color_column;
    @FXML private TableColumn<Team, String> team_textc_column;
    @FXML private TableColumn<Team, String> team_link_column;
    @FXML private TextField team_name;
    @FXML private ColorPicker team_color;
    @FXML private Circle team_color_circle;
    @FXML private ColorPicker team_textc;
    @FXML private Circle team_textc_circle;
    @FXML private TextField team_link;
    @FXML private Button team_add;
    @FXML private Button team_clear;
    @FXML private Button team_delet;
    //project tab students
    @FXML private TableView student_table;
    @FXML private TableColumn<Student, String> student_fname_column;
    @FXML private TableColumn<Student, String> student_lname_column;
    @FXML private TableColumn<Student, String> student_team_column;
    @FXML private TableColumn<Student, String> student_role_column;
    @FXML private TextField student_fname;
    @FXML private TextField student_lname;
    @FXML private ComboBox student_team;
    @FXML private TextField student_role;
    @FXML private Button student_add;
    @FXML private Button student_clear;
    @FXML private Button student_delet;
    
    
    //Main construct
    CourseSiteGenerator app;
    CsgController controller;
    CsgFileController fileController;
    
    //Things in TA Tab
    Boolean TAadd;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAadd =true;
        
        menu_file.setText(props.getProperty(CourseSiteGeneratorProp.MENU_FILE_TEXT));
        menu_edit.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EDIT_TEXT));
        menu_help.setText(props.getProperty(CourseSiteGeneratorProp.MENU_HELP_TEXT));
        menu_file_new.setText(props.getProperty(CourseSiteGeneratorProp.MENU_NEW_TEXT));
        menu_file_save.setText(props.getProperty(CourseSiteGeneratorProp.MENU_SAVE_TEXT));
        menu_file_saveAs.setText(props.getProperty(CourseSiteGeneratorProp.MENU_SAVEAS_TEXT));
        menu_file_load.setText(props.getProperty(CourseSiteGeneratorProp.MENU_LOAD_TEXT));
        menu_file_export.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EXPORT_TEXT));
        menu_file_exit.setText(props.getProperty(CourseSiteGeneratorProp.MENU_EXIT_TEXT));
        menu_edit_undo.setText(props.getProperty(CourseSiteGeneratorProp.MENU_UNDO_TEXT));
        menu_edit_redo.setText(props.getProperty(CourseSiteGeneratorProp.MENU_REDO_TEXT));
        menu_help_about.setText(props.getProperty(CourseSiteGeneratorProp.MENU_ABOUT_TEXT));
        
        tab_courseDetails.setText(props.getProperty(CourseSiteGeneratorProp.TAB_COURSEDETAILS_TEXT));
        tab_taData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_TADATA_TEXT));
        tab_recitationData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_RECITATIONDATA_TEXT));
        tab_scheduleData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_SCHEDULEDATA_TEXT));
        tab_projectData.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));

//        course_title.setText(props.getProperty(CourseSiteGeneratorProp.COURSE_TITLE));
//        course_subject_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_number_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_semester_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_year_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_title_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_intru_name_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_intru_home_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        export_dir_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_site_title.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_site_note.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_site_page_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_style_title.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_school_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_left_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_right_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_style_text.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_style_note.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        export_dir.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
//        course_emplate_dir.setText(props.getProperty(CourseSiteGeneratorProp.TAB_PROJECTDATA_TEXT));
        
        
        OfficeHourTitle.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_TITLE_TEXT));
        OfficeHourStart.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_START_TEXT));
        OfficeHourEnd.setText(props.getProperty(CourseSiteGeneratorProp.OFFICEHOUR_END_TEXT));
        
        TA_taTitle.setText(props.getProperty(CourseSiteGeneratorProp.TA_TITLE_TEXT));
        TAundergradColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_UNDERGRAD_CLUM_TEXT));
        TAnameColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_NAME_CLUM_TEXT));
        TAemailColumn.setText(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_CLUM_TEXT));
        TAnameinput.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_NAME_INPUT_TEXT));
        TAemailinput.setPromptText(props.getProperty(CourseSiteGeneratorProp.TA_EMAIL_INPUT_TEXT));
        TAaddButton.setText(props.getProperty(CourseSiteGeneratorProp.TA_ADD_BUTTON_TEXT));
        TAclearButton.setText(props.getProperty(CourseSiteGeneratorProp.TA_CLEAR_BUTTON_TEXT));
//        TAundergradColumn.setCellFactory( e -> new CheckBoxTableCell<>());
        
        
        //////
        CourseSiteGenerator.setWorkspace(this);
        CourseSiteGenerator.getStyle().initStylesheet();
        controller = new CsgController(app);
        fileController = new CsgFileController(app);
        CourseSiteGenerator.setFileController(fileController);
        
        //////
        menu_file_new.setOnAction(e -> {
        });
        menu_file_save.setOnAction(e -> {
            fileController.handleSaveRequest();
        });
        menu_file_saveAs.setOnAction(e -> {
            fileController.handleSaveAsRequest();
        });
        menu_file_load.setOnAction(e -> {
            fileController.handleLoadRequest();
        });
        menu_file_export.setOnAction(e -> {
            fileController.handleExportRequest();
        });
        menu_file_new.setOnAction(e -> {
        });
        menu_edit_undo.setOnAction(e -> {
            controller.Undo();
        });
        menu_edit_redo.setOnAction(e -> {
            controller.Redo();
        });
        //////Things in course tab
        CsgCourseData CourseData = app.getData().getCourseData();
        export_dir.setText(CourseData.getExport_dir());
        
        course_subject.setItems(FXCollections.observableArrayList("CSE", "AMS", "MAT"));
        course_subject.getSelectionModel().select(0);
        course_semester.setItems(FXCollections.observableArrayList("Spring", "Summer", "Fall", "Winter"));
        course_semester.getSelectionModel().select(0);
        course_number.setItems(FXCollections.observableArrayList("219", "380", "381"));
        course_number.getSelectionModel().select(0);
        course_year.setItems(FXCollections.observableArrayList("2017", "2018", "2019"));
        course_year.getSelectionModel().select(0);
        course_subject.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                controller.changeSubject(t, t1);
            }
        });
        course_semester.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                controller.changeSemester(t, t1);
            }
        });
        course_number.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                controller.changeNumber(t, t1);
            }
        });
        course_year.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                controller.changeYear(t, t1);
            }
        });
        course_title.setOnAction(e ->{
            controller.changeTitle(course_title.getText());
        });
        course_instru_name.setOnAction(e ->{
            controller.changeIntruName(course_instru_name.getText());
        });
        course_instru_home.setOnAction(e ->{
            controller.changeIntruHome(course_instru_home.getText());
        });
        export_dir_button.setOnAction(e ->{
            controller.changeExpoDir();
            export_dir.setText(app.getData().getCourseData().getExport_dir());
        });
        course_site_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<CourseSites> SiteTableData = FXCollections.observableArrayList();
        SiteTableData.add(new CourseSites("Home", "index.html", "HomeBuilder.js"));
        SiteTableData.add(new CourseSites("Syllabus", "syllabus.html", "SyllabusBuilder.js"));
        SiteTableData.add(new CourseSites("Schedule", "schedule.html", "ScheduleBuilder.js"));
//        SiteTableData.add(new CourseSites("HWs", "hws.html", "HWsBuilder.js"));
        SiteTableData.add(new CourseSites("Projects", "projects.html", "ProjectsBuilder.js"));
        course_site_table.setItems(SiteTableData);
        course_use_column.setCellValueFactory(cellData ->cellData.getValue().getCheckb());
        course_title_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitle()));
        course_file_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFile()));
        course_script_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getScript()));
        
        course_school_button.setOnAction(e -> {
            controller.changeSchoolImage();
            course_school_image.setImage(new Image("file:///" + CourseData.getSchool_image()));
        });
        course_left_button.setOnAction(e -> {
            controller.changeLeftImage();
            course_left_image.setImage(new Image("file:///" + CourseData.getLeft_image()));
        });
        course_right_button.setOnAction(e -> {
            controller.changeRightImage();
            course_right_image.setImage(new Image("file:///" + CourseData.getRight_image()));
        });
        
        //////TA tab things
        TA_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CsgTAData TAdata = CourseSiteGenerator.getData().getTAData();
        ObservableList<TeachingAssistant> TAtableData = TAdata.getTeachingAssistants();
        TA_table.setItems(TAtableData);
        TAundergradColumn.setCellValueFactory(cellData ->cellData.getValue().getCheckb());
        TAnameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        TAemailColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEmail()));
        
        TA_table.setOnMouseClicked(e -> {
            TeachingAssistant temp = (TeachingAssistant)TA_table.getSelectionModel().getSelectedItem();
            TAnameinput.setText(temp.getName());
            TAemailinput.setText(temp.getEmail());
        });
        //
        
        time_options = FXCollections.observableArrayList(
            "12 am", "1 am", "2 am", "3 am", "4 am", "5 am", "6 am", "7 am", "8 am", "9 am", "10 am", "11 am",
            "12 pm", "1 pm", "2 pm", "3 pm", "4 pm", "5 pm", "6 pm", "7 pm", "8 pm", "9 pm", "10 pm", "11 pm"
        );
        OfficeHourStartBox.setItems(time_options);
        OfficeHourEndBox.setItems(time_options);
//        OfficeHourStartBox = new ComboBox(time_options);
//        OfficeHourEndBox = new ComboBox(time_options);
        
        OfficeHourStartBox.getSelectionModel().select(TAdata.getStartHour());
        OfficeHourStartBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(t != null && t1 != null)
                    if(OfficeHourStartBox.getSelectionModel().getSelectedIndex() != TAdata.getStartHour())
                        controller.changeTime();
            }
        });
        OfficeHourEndBox.getSelectionModel().select(TAdata.getEndHour());
        OfficeHourEndBox.valueProperty().addListener(new ChangeListener<String>() {
            @Override public void changed(ObservableValue ov, String t, String t1) {
                if(t != null && t1 != null)
                    if(OfficeHourEndBox.getSelectionModel().getSelectedIndex() != TAdata.getEndHour())
                        controller.changeTime();
            }
        });
        
        
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();
        
        
        TAdata.initHours("9", "20");
        // CONTROLS FOR ADDING TAs
        TAnameinput.setOnAction(e -> {
            controller.handleAddTA();
        });
        TAemailinput.setOnAction(e -> {
            controller.handleAddTA();
        });
        TAaddButton.setOnAction(e -> {
            if(!TAadd)
                controller.changeExistTA();
            else
                controller.handleAddTA();
        });
        TAclearButton.setOnAction(e -> {
            TAaddButton.setText(props.getProperty(CourseSiteGeneratorProp.ADD_BUTTON_TEXT.toString()).toString());
            TAadd = true;
            TAnameinput.clear();
            TAemailinput.clear();
            TA_table.getSelectionModel().select(null);
        });

//        TAtable.setFocusTraversable(true);
//        TAtable.setOnKeyPressed(e -> {
//            controller.handleKeyPress(e.getCode());
//        });
        
        ///////Recitaion tab things
        recitation_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CsgRecitationData RecitationData = CourseSiteGenerator.getData().getRecitationData();
        ObservableList<Recitation> RecitationTableData = RecitationData.getRecitations();
        recitation_table.setItems(RecitationTableData);
        recitation_section_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSection()));
        recitation_instructor_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getInstructor()));
        recitation_time_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTime()));
        recitation_location_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLocation()));
        recitation_TA1_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTA1()));
        recitation_TA2_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTA2()));
        
        recitation_TA1.setItems(TAdata.getTeachingAssistants());
        recitation_TA2.setItems(TAdata.getTeachingAssistants());
        
        recitation_add.setOnAction(e -> {
            String section = recitation_section.getText();
            String instructor = recitation_instructor.getText();
            String time = recitation_time.getText();
            String location = recitation_location.getText();
            String TA1 = (recitation_TA1.getSelectionModel().getSelectedItem() != null)?recitation_TA1.getSelectionModel().getSelectedItem().toString():"";
            String TA2 = (recitation_TA2.getSelectionModel().getSelectedItem() != null)?recitation_TA2.getSelectionModel().getSelectedItem().toString():"";
            controller.addRecitation(section, instructor, time, location, TA1, TA2);
            recitation_section.clear();
            recitation_instructor.clear();
            recitation_time.clear();
            recitation_location.clear();
        });
        recitation_clear.setOnAction(e -> {
            recitation_section.clear();
            recitation_instructor.clear();
            recitation_time.clear();
            recitation_location.clear();
            recitation_TA1.getSelectionModel().select(0);
            recitation_TA2.getSelectionModel().select(0);
        });
        recitation_delet.setOnAction(e -> {
            String temp = ((Recitation)recitation_table.getSelectionModel().getSelectedItem()).getSection();
            controller.deletRecitation(temp);
        });
        recitation_table.setOnMouseClicked(e -> {
            Recitation temp = ((Recitation)recitation_table.getSelectionModel().getSelectedItem());
            recitation_section.setText(temp.getSection());
            recitation_instructor.setText(temp.getInstructor());
            recitation_time.setText(temp.getTime());
            recitation_location.setText(temp.getLocation());
        });
        
        //schedule tab things
        CsgSchedulesData ScheduleData = app.getData().getScheduleData();
        schedule_start.setValue(LocalDate.now());
        schedule_start.setOnAction(e -> {
            if(schedule_start.getValue().isAfter(ScheduleData.getEnd())){
                schedule_start.setValue(ScheduleData.getStart());
            } else {
                controller.changeScStart(schedule_start.getValue());
            }
        });
        schedule_end.setValue(LocalDate.now());
        schedule_end.setOnAction(e -> {
            if(schedule_end.getValue().isBefore(ScheduleData.getStart())){
                schedule_end.setValue(ScheduleData.getEnd());
            } else {
                controller.changeScEnd(schedule_end.getValue());
            }
        });
        
        schedule_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        schedule_table.setItems(ScheduleData.getSchedules());
        schedule_type_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getType()));
        schedule_date_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate().toString()));
        schedule_title_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTitle()));
        schedule_topic_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTopic()));
        
        ObservableList<String> schedule_options;
        schedule_options = FXCollections.observableArrayList(
            "Lecture", "Holiday", "HW", "Event"
        );
        schedule_type.setItems(schedule_options);
        
        schedule_add.setOnAction(e -> {
            String type = schedule_type.getSelectionModel().getSelectedItem().toString();
            LocalDate date = schedule_date.getValue();
            String time = schedule_time.getText();
            String title = schedule_title.getText();
            String topic = schedule_topic.getText();
            String link = schedule_link.getText();
            String criteria = schedule_criteria.getText();
            controller.addSchedule(type, date, time, title, topic, link, criteria);
            schedule_type.getSelectionModel().select(0);
            schedule_date.setValue(LocalDate.now());
            schedule_time.clear();
            schedule_title.clear();
            schedule_topic.clear();
            schedule_link.clear();
            schedule_criteria.clear();
        });
        schedule_clear.setOnAction(e -> {
            schedule_type.getSelectionModel().select(0);
            schedule_date.setValue(LocalDate.now());
            schedule_time.clear();
            schedule_title.clear();
            schedule_topic.clear();
            schedule_link.clear();
            schedule_criteria.clear();
        });
        schedule_delet.setOnAction(e -> {
            Schedule temp = ((Schedule)schedule_table.getSelectionModel().getSelectedItem());
            LocalDate date = temp.getDate();
            controller.deletSchedule(date);
        });
        
        schedule_table.setOnMouseClicked(e -> {
            Schedule temp = ((Schedule)schedule_table.getSelectionModel().getSelectedItem());
            schedule_type.getSelectionModel().select(temp.getType());
            schedule_date.setValue(temp.getDate());
            schedule_time.setText(temp.getTime());
            schedule_title.setText(temp.getTitle());
            schedule_topic.setText(temp.getTopic());
            schedule_link.setText(temp.getLink());
            schedule_criteria.setText(temp.getCriteria());
        });
        
        ///project tab team
        CsgTeamData TeamData = app.getData().getTeamData();
        team_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        team_table.setItems(TeamData.getTeams());
        team_name_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        team_color_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getColor().toString()));
        team_textc_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getText_color().toString()));
        team_link_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLink()));
        
        team_color.setOnAction(e -> {
            team_color_circle.setFill(team_color.getValue());
        });
        team_textc.setOnAction(e -> {
            team_textc_circle.setFill(team_textc.getValue());
        });
        
        team_add.setOnAction(e -> {
            String name = team_name.getText();
            Color color = team_color.getValue();
            Color textc = team_textc.getValue();
            String link = team_link.getText();
            controller.addTeam(name, color, textc, link);
            team_name.clear();
            team_color.setValue(Color.BLUE);
            team_textc.setValue(Color.BLUE);
            team_color_circle.setFill(Color.BLUE);
            team_textc_circle.setFill(Color.BLUE);
            team_link.clear();
        });
        team_clear.setOnAction(e -> {
            team_name.clear();
            team_color.setValue(Color.BLUE);
            team_textc.setValue(Color.BLUE);
            team_color_circle.setFill(Color.BLUE);
            team_textc_circle.setFill(Color.BLUE);
            team_link.clear();
        });
        team_delet.setOnAction(e -> {
            Team temp = (Team)team_table.getSelectionModel().getSelectedItem();
            controller.deletTeam(temp.getName());
        });
        team_table.setOnMouseClicked(e -> {
            Team temp = (Team)team_table.getSelectionModel().getSelectedItem();
            team_name.setText(temp.getName());
            team_color.setValue(temp.getColor());
            team_color_circle.setFill(temp.getColor());
            team_textc.setValue(temp.getText_color());
            team_textc_circle.setFill(temp.getText_color());
            team_link.setText(temp.getLink());
        });
        //project tab students
        CsgStudentData StudentData = app.getData().getStudentData();
        student_table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        student_table.setItems(StudentData.getStudents());
        student_fname_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFname()));
        student_lname_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLname()));
        student_team_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTeam()));
        student_role_column.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRole()));
        
        student_team.setItems(TeamData.getTeams());
        
        student_add.setOnAction(e -> {
            String fname = student_fname.getText();
            String lname = student_lname.getText();
            String team;
            if(student_team.getSelectionModel().getSelectedItem() != null)
                team = student_team.getSelectionModel().getSelectedItem().toString();
            else
                team = "No team";
            String role = student_role.getText();
            controller.addStudent(fname, lname, team, role);
            student_fname.clear();
            student_lname.clear();
            student_team.getSelectionModel().select(0);
            student_role.clear();
        });
        student_clear.setOnAction(e -> {
            student_fname.clear();
            student_lname.clear();
            student_team.getSelectionModel().select(0);
            student_role.clear();
        });
        student_delet.setOnAction(e -> {
            Student temp = (Student)student_table.getSelectionModel().getSelectedItem();
            controller.deletStudent(temp.getFname(), temp.getLname());
        });
        student_table.setOnMouseClicked(e -> {
            Student temp = (Student)student_table.getSelectionModel().getSelectedItem();
            student_fname.setText(temp.getFname());
            student_lname.setText(temp.getLname());
            student_team.getSelectionModel().select(temp.getTeam());
            student_role.setText(temp.getRole());
        });
    }     
    
//    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
//    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
//

    public AnchorPane getMainPane() {
        return MainPane;
    }
    
    public TableView getTATable() {
        return TA_table;
    }

    public GridPane getOfficeHoursGridPane() {
        return OfficeHoursGridPane;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }
    
    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }
    
    public TextField getNameTextField(){
        return TAnameinput;
    }
    
     public TextField getEmailTextField(){
        return TAemailinput;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public ComboBox getOfficeHourStartBox() {
        return OfficeHourStartBox;
    }

    public void setOfficeHourStartBox(ComboBox OfficeHourStartBox) {
        this.OfficeHourStartBox = OfficeHourStartBox;
    }

    public ComboBox getOfficeHourEndBox() {
        return OfficeHourEndBox;
    }

    public void setOfficeHourEndBox(ComboBox OfficeHourEndBox) {
        this.OfficeHourEndBox = OfficeHourEndBox;
    }

    public TableView getCourse_site_table() {
        return course_site_table;
    }
    
    
    
    ////

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public void resetWorkspace() {
        // CLEAR OUT THE GRID PANE
        OfficeHoursGridPane.getChildren().clear();
        
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }
    
    public void reloadWorkspace(CsgData dataComponent) {
        CsgData data = dataComponent;
        course_subject.getSelectionModel().select(data.getCourseData().getCourse_subject());
        course_semester.getSelectionModel().select(data.getCourseData().getCourse_semester());
        course_year.getSelectionModel().select(data.getCourseData().getCourse_year());
        course_number.getSelectionModel().select(data.getCourseData().getCourse_num());
        course_title.setText(data.getCourseData().getCourse_title());
        course_instru_name.setText(data.getCourseData().getInstructor_name());
        course_instru_home.setText(data.getCourseData().getInstructor_home());
        export_dir.setText(data.getCourseData().getExport_dir());
        
        reloadOfficeHoursGrid(data.getTAData());
        OfficeHourStartBox.getSelectionModel().select(data.getTAData().getStartHour());
        OfficeHourEndBox.getSelectionModel().select(data.getTAData().getEndHour());
        
        recitation_section.clear();
        recitation_instructor.clear();
        recitation_time.clear();
        recitation_location.clear();
        recitation_TA1.getSelectionModel().select(0);
        recitation_TA2.getSelectionModel().select(0);
        
        schedule_start.setValue(dataComponent.getScheduleData().getStart());
        schedule_end.setValue(dataComponent.getScheduleData().getEnd());
        schedule_type.getSelectionModel().select(0);
        schedule_date.setValue(LocalDate.now());
        schedule_time.clear();
        schedule_title.clear();
        schedule_topic.clear();
        schedule_link.clear();
        schedule_criteria.clear();
        
        team_name.clear();
        team_color.setValue(Color.BLUE);
        team_textc.setValue(Color.BLUE);
        team_color_circle.setFill(Color.BLUE);
        team_textc_circle.setFill(Color.BLUE);
        team_link.clear();
        
        student_fname.clear();
        student_lname.clear();
        student_team.getSelectionModel().select(0);
        student_role.clear();
        
    }

    public void reloadOfficeHoursGrid(CsgTAData dataComponent) {        
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0, "office_hours_grid_time_column_header_pane", "office_hours_grid_time_column_header_label");
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }
        
        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0, "office_hours_grid_day_column_header_pane", "office_hours_grid_day_column_header_label");
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));            
        }
        
        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row, "office_hours_grid_time_cell_pane", "office_hours_grid_time_cell_label");
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1, "office_hours_grid_time_cell_pane", "office_hours_grid_time_cell_label");
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row, "office_hours_grid_time_cell_pane", "office_hours_grid_time_cell_label");
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row+1, "office_hours_grid_time_cell_pane", "office_hours_grid_time_cell_label");
            dataComponent.getCellTextProperty(col, row+1).set(buildCellText(endHour+1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row, "office_hours_grid_ta_cell_pane", "office_hours_grid_ta_cell_label");
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row+1, "office_hours_grid_ta_cell_pane", "office_hours_grid_ta_cell_label");
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
//            p.setOnMouseExited(e -> {
//                controller.handleGridCellMouseExited((Pane) e.getSource());
//            });
//            p.setOnMouseEntered(e -> {
//                controller.handleGridCellMouseEntered((Pane) e.getSource());
//            });
        }
        
        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
//        TAStyle taStyle = (TAStyle)app.getStyleComponent();
//        taStyle.initOfficeHoursGridStyle();
    }
    
    public void addCellToGrid(CsgTAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row, String paneStyle, String labelStyle) {       
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);
        cellPane.getStyleClass().add(paneStyle);
        cellLabel.getStyleClass().add(labelStyle);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);
        
        // NOW PUT THE CELL IN THE WORKSPACE GRID
        OfficeHoursGridPane.add(cellPane, col, row);
        
        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);
        
        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());        
    }
    
    
}
