/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import javafx.beans.InvalidationListener;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.CheckBox;

/**
 *
 * @author zhaotingyi
 */
public class CourseSites {
    private CheckBox checkb;
    private BooleanProperty check;
//    private final StringProperty Title;
    private final String Title;
    private StringProperty File;
    private StringProperty Script;
    
    public CourseSites(String Title, String File, String Script){
        check = new SimpleBooleanProperty(true);
        checkb = new CheckBox();
        checkb.selectedProperty().set(true);
//        this.Title = new SimpleStringProperty(Title);
        this.Title = Title;
        this.File = new SimpleStringProperty(File);
        this.Script = new SimpleStringProperty(Script);
    }

    public Boolean getCheck() {
        return check.get();
    }

    public void setCheck(Boolean check) {
        this.check.set(check);
    }

    public String getTitle() {
//        return Title.get();
        return Title;
    }

    public String getFile() {
        return File.get();
    }

    public void setFile(StringProperty File) {
        this.File = File;
    }

    public String getScript() {
        return Script.get();
    }

    public void setScript(StringProperty Script) {
        this.Script = Script;
    }

//    public CheckBox getCheckb() {
//        return checkb;
//    }

    public void setCheckb(CheckBox checkb) {
        this.checkb = checkb;
    }
    
    public ObservableValue<CheckBox> getCheckb()
    {
        return new  ObservableValue<CheckBox>() {
            @Override
            public void addListener(ChangeListener<? super CheckBox> listener) {

            }

            @Override
            public void removeListener(ChangeListener<? super CheckBox> listener) {

            }

            @Override
            public CheckBox getValue() {
                return checkb;
            }

            @Override
            public void addListener(InvalidationListener listener) {

            }

            @Override
            public void removeListener(InvalidationListener listener) {

            }
        };
    }
    
    public Boolean isSelected()
    {
        return checkb.isSelected();
    }
    
    
}
