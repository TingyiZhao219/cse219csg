/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.workspace;

import csg.CourseSiteGenerator;
import csg.CourseSiteGeneratorProp;
import csg.data.CsgRecitationData;
import csg.data.CsgStudentData;
import csg.data.CsgTAData;
import csg.data.TeachingAssistant;
import csg.file.TimeSlot;
import csg.jtps.CourseIntruHomeUR;
import csg.jtps.CourseIntruNameUR;
import csg.jtps.CourseNumberUR;
import csg.jtps.CourseSemesterUR;
import csg.jtps.CourseSubjectUR;
import csg.jtps.CourseTitleUR;
import csg.jtps.CourseYearUR;
import csg.jtps.RCAdderUR;
import csg.jtps.RCDeletUR;
import csg.jtps.RCEditerUR;
import csg.jtps.SCAdderUR;
import csg.jtps.SCDeletUR;
import csg.jtps.SCEditerUR;
import csg.jtps.STAdderUR;
import csg.jtps.STDeletUR;
import csg.jtps.STEditerUR;
import csg.jtps.TAAdderUR;
import csg.jtps.TAReplaceUR;
import csg.jtps.TAdeletUR;
import csg.jtps.TAhourschangeUR;
import csg.jtps.TAtoggleUR;
import csg.jtps.TeamAdderUR;
import csg.jtps.TeamDeletUR;
import csg.jtps.TeamEditerUR;
import csg.ui.AppMessageDialogSingleton;
import csg.ui.AppYesNoCancelDialogSingleton;
import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CsgController {
    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    CourseSiteGenerator app;
    static jTPS jTPS = new jTPS();

    /**
     * Constructor, note that the app must already be constructed.
     * @param initApp
     */
    public CsgController(CourseSiteGenerator initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }
    
    //Course tab handlers
    public void changeSubject(String pre_sub, String sub){
        jTPS_Transaction addTAUR = new CourseSubjectUR(app, pre_sub, sub);
        jTPS.addTransaction(addTAUR);
    }
    public void changeSemester(String pre_sub, String sub){
        jTPS_Transaction addTAUR = new CourseSemesterUR(app, pre_sub, sub);
        jTPS.addTransaction(addTAUR);
    }
    public void changeNumber(String pre_sub, String sub){
        jTPS_Transaction addTAUR = new CourseNumberUR(app, pre_sub, sub);
        jTPS.addTransaction(addTAUR);
    }
    public void changeYear(String pre_sub, String sub){
        jTPS_Transaction addTAUR = new CourseYearUR(app, pre_sub, sub);
        jTPS.addTransaction(addTAUR);
    }
    public void changeTitle(String new_tit){
        jTPS_Transaction addTAUR = new CourseTitleUR(app, new_tit);
        jTPS.addTransaction(addTAUR);
    }
    public void changeIntruName(String new_name){
        jTPS_Transaction addTAUR = new CourseIntruNameUR(app, new_name);
        jTPS.addTransaction(addTAUR);
    }
    public void changeIntruHome(String new_home){
        jTPS_Transaction addTAUR = new CourseIntruHomeUR(app, new_home);
        jTPS.addTransaction(addTAUR);
    }
    public void changeExpoDir(){
        DirectoryChooser dc = new DirectoryChooser();
        dc.setInitialDirectory(new File("./"));
        String path = dc.showDialog(null).getPath();
        app.getData().getCourseData().setExport_dir(path);
    }
    public void changeSchoolImage(){
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./"));
	fc.setTitle("Choos your image.");
        File selectedFile = fc.showOpenDialog(app.getInitStage());
        if(selectedFile != null){
            String path = selectedFile.getPath();
            app.getData().getCourseData().setSchool_image(path);
        }
    }
    public void changeLeftImage(){
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./"));
	fc.setTitle("Choos your image.");
        File selectedFile = fc.showOpenDialog(app.getInitStage());
        if(selectedFile != null){
            String path = selectedFile.getPath();
            app.getData().getCourseData().setLeft_image(path);
        }
    }
    public void changeRightImage(){
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./"));
	fc.setTitle("Choos your image.");
        File selectedFile = fc.showOpenDialog(app.getInitStage());
        if(selectedFile != null){
            String path = selectedFile.getPath();
            app.getData().getCourseData().setRight_image(path);
        }
    }
    //TA tab handlers
    /**
     * This method responds to when the user requests to add
     * a new TA via the UI. Note that it must first do some
     * validation to make sure a unique name and email address
     * has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        CsgWorkspace workspace = (CsgWorkspace)app.getWorkspace();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        CsgTAData data = (CsgTAData)app.getData().getTAData();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {           
        }
        // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {                     
        }
        // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {                                  
        }
        // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction addTAUR = new TAAdderUR(app);
            jTPS.addTransaction(addTAUR);
            
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");
            
            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            
        }
    }

    /**
     * This function provides a response for when the user presses a
     * keyboard key. Note that we're only responding to Delete, to remove
     * a TA.
     * 
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?
        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            CsgWorkspace workspace = app.getWorkspace();
            TableView taTable = workspace.getTATable();
            
            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                // GET THE TA AND REMOVE IT
                TeachingAssistant ta = (TeachingAssistant)selectedItem;
                String taName = ta.getName();
                CsgTAData data = app.getData().getTAData();
                
                jTPS_Transaction deletUR = new TAdeletUR(app, taName);
                jTPS.addTransaction(deletUR);
                
                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
                // WE'VE CHANGED STUFF
//                markWorkAsEdited();
            }
        }
    }

    /**
     * This function provides a response for when the user clicks
     * on the office hours grid to add or remove a TA to a time slot.
     * 
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        CsgWorkspace workspace = app.getWorkspace();
        TableView taTable = workspace.getTATable();
        
        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            // GET THE TA
            TeachingAssistant ta = (TeachingAssistant)selectedItem;
            String taName = ta.getName();
            CsgTAData data = app.getData().getTAData();
            String cellKey = pane.getId();
            
            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            jTPS_Transaction toggleUR = new TAtoggleUR(taName, cellKey, data);
            jTPS.addTransaction(toggleUR);
            
        }
    }
    
    public void changeTime(){
        CsgTAData TAdata = (CsgTAData)app.getData().getTAData();
        CsgWorkspace workspace = (CsgWorkspace)app.getWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ComboBox comboBox1 = workspace.getOfficeHourStartBox();
        ComboBox comboBox2 = workspace.getOfficeHourEndBox();
        int startTime = TAdata.getStartHour();
        int endTime = TAdata.getEndHour();
        int newStartTime = comboBox1.getSelectionModel().getSelectedIndex();
        int newEndTime = comboBox2.getSelectionModel().getSelectedIndex();
        if(newStartTime > endTime || newEndTime < startTime){
            comboBox1.getSelectionModel().select(startTime);
            comboBox2.getSelectionModel().select(endTime);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(CourseSiteGeneratorProp.START_OVER_END_TITLE.toString()), props.getProperty(CourseSiteGeneratorProp.START_OVER_END_MESSAGE.toString()));
            return;
        }
        ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(TAdata);
        if(officeHours.isEmpty()){
            workspace.getOfficeHoursGridPane().getChildren().clear();
            TAdata.initHours("" + newStartTime, "" + newEndTime);
        } else {
        officeHours = TimeSlot.buildOfficeHoursList(TAdata);
        TimeSlot temp = officeHours.get(0);
        String firsttime = officeHours.get(0).getTime();
        int firsthour = Integer.parseInt(firsttime.substring(0, firsttime.indexOf('_')));
        if(firsttime.contains("pm"))
            firsthour += 12;
        if(firsttime.contains("12"))
            firsthour -= 12;
        String lasttime = officeHours.get(officeHours.size() - 1).getTime();
        int lasthour = Integer.parseInt(lasttime.substring(0, lasttime.indexOf('_')));
        if(lasttime.contains("pm"))
            lasthour += 12;
        if(lasttime.contains("12"))
            lasthour -= 12;
        if(firsthour < newStartTime || lasthour + 1 > newEndTime){
            AppYesNoCancelDialogSingleton yesNoDialog = AppYesNoCancelDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(CourseSiteGeneratorProp.OFFICE_HOURS_REMOVED_TITLE.toString()), props.getProperty(CourseSiteGeneratorProp.OFFICE_HOURS_REMOVED_MESSAGE).toString());
            String selection = yesNoDialog.getSelection();
            if (!selection.equals(AppYesNoCancelDialogSingleton.YES)){
                comboBox1.getSelectionModel().select(startTime);
                comboBox2.getSelectionModel().select(endTime);
                return;
            }
        }
        }
        jTPS_Transaction changeTimeUR = new TAhourschangeUR(app);
        jTPS.addTransaction(changeTimeUR);
    }
     
    public void changeExistTA(){
        CsgWorkspace workspace = app.getWorkspace();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        CsgTAData data = app.getData().getTAData();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        String name = ta.getName();
        String newName = workspace.getNameTextField().getText();
        String newEmail = workspace.getEmailTextField().getText();
        jTPS_Transaction replaceTAUR = new TAReplaceUR(app);
        jTPS.addTransaction(replaceTAUR);
    }
    
    //Recitation Tab handlers
    
    public void addRecitation(String section, String instructor, String time, String location, String TA1, String TA2){
        CsgRecitationData data = app.getData().getRecitationData();
        if(!data.containsRecitation(section)){
            jTPS_Transaction addRecitation = new RCAdderUR(app, section, instructor, time, location, TA1, TA2);
            jTPS.addTransaction(addRecitation);
        } else {
            jTPS_Transaction editRecitation = new RCEditerUR(app, section, instructor, time, location, TA1, TA2);
            jTPS.addTransaction(editRecitation);
        }
    }
    public void deletRecitation(String section){
            jTPS_Transaction deletRecitation = new RCDeletUR(app, section);
            jTPS.addTransaction(deletRecitation);
        
    }
    
    //Schedule Tab handlers
    
    public void changeScStart(LocalDate time){
        app.getData().getScheduleData().setStart(time);
    }
    public void changeScEnd(LocalDate time){
        app.getData().getScheduleData().setEnd(time);
    }
    public void addSchedule(String type, LocalDate date, String time, String title, String topic, String link, String criteria){
        if(date.isBefore(app.getData().getScheduleData().getStart()) || date.isAfter(app.getData().getScheduleData().getEnd())){
            return;
        }
        if(!app.getData().getScheduleData().containsSchedule(date)){
            jTPS_Transaction addSchedule = new SCAdderUR(app, type, date, time, title, topic, link, criteria);
            jTPS.addTransaction(addSchedule);
        } else {
            jTPS_Transaction editSchedule = new SCEditerUR(app, type, date, time, title, topic, link, criteria);
            jTPS.addTransaction(editSchedule);
        }
    }
    public void deletSchedule(LocalDate date){
        jTPS_Transaction deletSchedule = new SCDeletUR(app, date);
        jTPS.addTransaction(deletSchedule);
    }
    
    //Project tab team handlers
    
    public void addTeam(String name, Color color, Color textc, String link){
        if(!app.getData().getTeamData().containsTeam(name)){
            jTPS_Transaction addTeam = new TeamAdderUR(app, name, color, textc, link);
            jTPS.addTransaction(addTeam);
        } else {
            jTPS_Transaction editTeam = new TeamEditerUR(app, name, color, textc, link);
            jTPS.addTransaction(editTeam);
        }
    }
    public void deletTeam(String name){
        jTPS_Transaction deletTeam = new TeamDeletUR(app, name);
        jTPS.addTransaction(deletTeam);
    }
    
    //project tab students
    
    public void addStudent(String fname, String lname, String team, String role){
        CsgStudentData data = app.getData().getStudentData();
        if(data.containsStudent(fname, lname)){
            jTPS_Transaction editStudent = new STEditerUR(app, fname, lname, team, role);
            jTPS.addTransaction(editStudent);
        } else {
            jTPS_Transaction addStudent = new STAdderUR(app, fname, lname, team, role);
            jTPS.addTransaction(addStudent);
        }
    }
    public void deletStudent(String fname, String lname){
        jTPS_Transaction deletStudent = new STDeletUR(app, fname, lname);
        jTPS.addTransaction(deletStudent);
        
    }
    
    public void Undo(){
        jTPS.undoTransaction();
    }
    public void Redo(){
        jTPS.doTransaction();
    }
}
