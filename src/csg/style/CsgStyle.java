/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.style;

import csg.CourseSiteGenerator;
import static csg.CourseSiteGeneratorProp.APP_STYLE_CSS;
import static csg.CourseSiteGeneratorProp.APP_STYLE_PATH_CSS;
import java.net.URL;
import properties_manager.PropertiesManager;

/**
 *
 * @author zhaotingyi
 */
public class CsgStyle {
    public static final String CLASS_BORDERED_PANE = "bordered_pane";
    public static final String CLASS_FILE_BUTTON = "file_button";
    private final CourseSiteGenerator app;
    
    public CsgStyle(CourseSiteGenerator initApp){
        app = initApp;
    }
    
    public void initStylesheet() {
	// SELECT THE STYLESHEET
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
//	String stylesheet = props.getProperty(APP_STYLE_PATH_CSS);
//	stylesheet += props.getProperty(APP_STYLE_CSS);
//        Class appClass = app.getWorkspace().getClass();
//	URL stylesheetURL = appClass.getResource(stylesheet);
//	String stylesheetPath = stylesheetURL.toExternalForm();
        String path = "file:/" + System.getProperty("user.dir").replace('\\', '/') + "/src/csg/style/Csg_style.css";
	app.getWorkspace().getMainPane().getStylesheets().add("file:/C:/Users/zhaotingyi/Documents/CSE219/CourseSiteGenerator/CourseSiteGenerator/src/csg/style/Csg_style.css");	
    }
}
