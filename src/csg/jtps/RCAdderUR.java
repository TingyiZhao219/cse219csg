/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class RCAdderUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String section;
    private String instructor;
    private String time;
    private String location;
    private String TA1;
    private String TA2;
    
    public RCAdderUR(CourseSiteGenerator app, String section, String instructor, String time, String location, String TA1, String TA2){
        this.app = app;
        this.section = section;
        this.instructor = instructor;
        this.time = time;
        this.location = location;
        this.TA1 = TA1;
        this.TA2 = TA2;
    }

    @Override
    public void doTransaction() {
        app.getData().getRecitationData().addRecitation(section, instructor, time, location, TA1, TA2);
    }

    @Override
    public void undoTransaction() {
        app.getData().getRecitationData().removeRecitation(section);
    }
    
}
