/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Team;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class TeamDeletUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String name;
    private Color color;
    private Color textc;
    private String link;
    
    public TeamDeletUR(CourseSiteGenerator app, String name){
        this.app = app;
        this.name = name;
        Team temp = app.getData().getTeamData().getTeam(name);
        color = temp.getColor();
        textc = temp.getText_color();
        link = temp.getLink();
    }

    @Override
    public void doTransaction() {
        app.getData().getTeamData().removeTeam(name);
    }

    @Override
    public void undoTransaction() {
        app.getData().getTeamData().addTeam(name, color, textc, link);
    }
    
}
