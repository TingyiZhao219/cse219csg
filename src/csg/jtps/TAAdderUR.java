/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.workspace.CsgWorkspace;
import java.util.regex.Pattern;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class TAAdderUR implements jTPS_Transaction{
    
    private String TAName;
    private String TAEmail;
    private CourseSiteGenerator app;
    private CsgWorkspace workspace;
    
    public TAAdderUR(CourseSiteGenerator app){
        this.app = app;
        workspace = app.getWorkspace();
        TAName = workspace.getNameTextField().getText();
        TAEmail = workspace.getEmailTextField().getText();
    }

    @Override
    public void doTransaction() {
        app.getData().getTAData().addTA(TAName, TAEmail);
    }

    @Override
    public void undoTransaction() {
        app.getData().getTAData().removeTA(TAName);
    }
    
}
