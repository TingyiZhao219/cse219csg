/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class STAdderUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String fname;
    private String lname;
    private String team;
    private String role;
    
    public STAdderUR(CourseSiteGenerator app, String fname, String lname, String team, String role){
        this.app = app;
        this.fname = fname;
        this.lname = lname;
        this.team = team;
        this.role = role;
    }

    @Override
    public void doTransaction() {
        app.getData().getStudentData().addStudent(fname, lname, team, role);
    }

    @Override
    public void undoTransaction() {
        app.getData().getStudentData().removeStudent(fname, lname);
    }
    
}
