/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.CsgCourseData;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class CourseSemesterUR implements jTPS_Transaction{
    private CourseSiteGenerator app; 
    private String pre_sub;
    private String sub;
    
    public CourseSemesterUR(CourseSiteGenerator app, String pre_sub, String sub){
        this.app = app;
        this.pre_sub = pre_sub;
        this.sub = sub;
    }

    @Override
    public void doTransaction() {
        CsgCourseData data = app.getData().getCourseData();
        data.setCourse_semester(sub);
    }

    @Override
    public void undoTransaction() {
        CsgCourseData data = app.getData().getCourseData();
        data.setCourse_semester(pre_sub);
    }
    
}
