/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.CsgTAData;
import csg.file.TimeSlot;
import csg.workspace.CsgWorkspace;
import java.util.ArrayList;
import javafx.scene.control.ComboBox;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
//import tam.TAManagerApp;
//import tam.data.TAData;
//import tam.file.TimeSlot;
//import tam.workspace.TAWorkspace;

/**
 *
 * @author zhaotingyi
 */
public class TAhourschangeUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private int startTime;
    private int endTime;
    private int newStartTime;
    private int newEndTime;
    private ArrayList<TimeSlot> officeHours;
    
    public TAhourschangeUR(CourseSiteGenerator app){
        this.app = app;
        CsgTAData data = (CsgTAData)app.getData().getTAData();
        CsgWorkspace workspace = (CsgWorkspace)app.getWorkspace();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ComboBox comboBox1 = workspace.getOfficeHourStartBox();
        ComboBox comboBox2 = workspace.getOfficeHourEndBox();
        startTime = data.getStartHour();
        endTime = data.getEndHour();
        newStartTime = comboBox1.getSelectionModel().getSelectedIndex();
        newEndTime = comboBox2.getSelectionModel().getSelectedIndex();
        officeHours = TimeSlot.buildOfficeHoursList(data);
    }

    @Override
    public void doTransaction() {
        app.getWorkspace().getOfficeHoursGridPane().getChildren().clear();
        app.getData().getTAData().changeTime(newStartTime, newEndTime, officeHours);
    }

    @Override
    public void undoTransaction() {
        app.getWorkspace().getOfficeHoursGridPane().getChildren().clear();
        app.getData().getTAData().changeTime(startTime, endTime, officeHours);
    }
    
}
