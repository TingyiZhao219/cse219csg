/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Recitation;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class RCEditerUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String section;
    private String instructor;
    private String pre_instructor;
    private String time;
    private String pre_time;
    private String location;
    private String pre_location;
    private String TA1;
    private String pre_TA1;
    private String TA2;
    private String pre_TA2;
    
    public RCEditerUR(CourseSiteGenerator app, String section, String instructor, String time, String location, String TA1, String TA2){
        this.app = app;
        this.section = section;
        this.instructor = instructor;
        this.time = time;
        this.location = location;
        this.TA1 = TA1;
        this.TA2 = TA2;
        Recitation temp = app.getData().getRecitationData().getRecitation(section);
        pre_instructor = temp.getInstructor();
        pre_time = temp.getTime();
        pre_location = temp.getLocation();
        pre_TA1 = temp.getTA1();
        pre_TA2 = temp.getTA2();
    }

    @Override
    public void doTransaction() {
        app.getData().getRecitationData().removeRecitation(section);
        app.getData().getRecitationData().addRecitation(section, instructor, time, location, TA1, TA2);
    }

    @Override
    public void undoTransaction() {
        app.getData().getRecitationData().removeRecitation(section);
        app.getData().getRecitationData().addRecitation(section, pre_instructor, pre_time, pre_location, pre_TA1, pre_TA2);
    }
    
}
