/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Student;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class STEditerUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String fname;
    private String lname;
    private String team;
    private String role;
    private String pre_team;
    private String pre_role;
    
    public STEditerUR(CourseSiteGenerator app, String fname, String lname, String team, String role){
        this.app = app;
        this.fname = fname;
        this.lname = lname;
        this.team = team;
        this.role = role;
        Student temp = app.getData().getStudentData().getStudent(fname, lname);
        pre_team = temp.getTeam();
        pre_role = temp.getRole();
    }

    @Override
    public void doTransaction() {
        app.getData().getStudentData().removeStudent(fname, lname);
        app.getData().getStudentData().addStudent(fname, lname, team, role);
    }

    @Override
    public void undoTransaction() {
        app.getData().getStudentData().removeStudent(fname, lname);
        app.getData().getStudentData().addStudent(fname, lname, pre_team, pre_role);
    }
    
}
