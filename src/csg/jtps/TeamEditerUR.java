/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Team;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class TeamEditerUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String name;
    private Color color;
    private Color textc;
    private String link;
    private Color pre_color;
    private Color pre_textc;
    private String pre_link;
    
    public TeamEditerUR(CourseSiteGenerator app, String name, Color color, Color textc, String link){
        this.app = app;
        this.name = name;
        this.color = color;
        this.textc = textc;
        this.link = link;
        Team temp = app.getData().getTeamData().getTeam(name);
        pre_color = temp.getColor();
        pre_textc = temp.getText_color();
        pre_link = temp.getLink();
    }

    @Override
    public void doTransaction() {
        app.getData().getTeamData().removeTeam(name);
        app.getData().getTeamData().addTeam(name, color, textc, link);
    }

    @Override
    public void undoTransaction() {
        app.getData().getTeamData().removeTeam(name);
        app.getData().getTeamData().addTeam(name, pre_color, pre_textc, pre_link);
    }
    
}
