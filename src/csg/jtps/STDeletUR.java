/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Student;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class STDeletUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String fname;
    private String lname;
    private String team;
    private String role;
    
    public STDeletUR(CourseSiteGenerator app, String fname, String lname){
        this.app = app;
        this.fname = fname;
        this.lname = lname;
        Student temp = app.getData().getStudentData().getStudent(fname, lname);
        team = temp.getTeam();
        role = temp.getRole();
    }

    @Override
    public void doTransaction() {
        app.getData().getStudentData().removeStudent(fname, lname);
    }

    @Override
    public void undoTransaction() {
        app.getData().getStudentData().addStudent(fname, lname, team, role);
    }
    
}
