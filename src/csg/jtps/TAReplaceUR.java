/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.CsgTAData;
import csg.data.TeachingAssistant;
import csg.workspace.CsgWorkspace;
import javafx.scene.control.TableView;
import jtps.jTPS_Transaction;
//import tam.TAManagerApp;
//import tam.data.TAData;
//import tam.data.TeachingAssistant;
//import tam.workspace.TAWorkspace;

/**
 *
 * @author zhaotingyi
 */
public class TAReplaceUR implements jTPS_Transaction{
    private String TAname;
    private String TAemail;
    private String newName;
    private String newEmail;
    private CourseSiteGenerator app;
    private CsgTAData data;
    
    public TAReplaceUR(CourseSiteGenerator app){
        this.app = app;
        CsgWorkspace workspace = app.getWorkspace();
        data = app.getData().getTAData();
        newName = workspace.getNameTextField().getText();
        newEmail = workspace.getEmailTextField().getText();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant)selectedItem;
        TAname = ta.getName();
        TAemail = ta.getEmail();
    }

    @Override
    public void doTransaction() {
        data.replaceTAName(TAname, newName);
        data.removeTA(TAname);
        data.addTA(newName, newEmail);
        CsgWorkspace workspace = app.getWorkspace();
        TableView taTable = workspace.getTATable();
        taTable.getSelectionModel().select(data.getTA(newName));
    }

    @Override
    public void undoTransaction() {
        data.replaceTAName(newName, TAname);
        data.removeTA(newName);
        data.addTA(TAname, TAemail);
        CsgWorkspace workspace = app.getWorkspace();
        TableView taTable = workspace.getTATable();
        taTable.getSelectionModel().select(data.getTA(TAname));
    }
    
    
    
}
