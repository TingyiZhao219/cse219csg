/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class SCAdderUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String type;
    private LocalDate date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    
    public SCAdderUR(CourseSiteGenerator app, String type, LocalDate date, String time, String title, String topic, String link, String criteria){
        this.app = app;
        this.type = type;
        this.date = date;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
    }

    @Override
    public void doTransaction() {
        app.getData().getScheduleData().addSchedule(type, date, time, title, topic, link, criteria);
    }

    @Override
    public void undoTransaction() {
        app.getData().getScheduleData().removeSchedule(date);
    }
    
}
