/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Schedule;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class SCEditerUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String type;
    private LocalDate date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    private String pre_type;
    private LocalDate pre_date;
    private String pre_time;
    private String pre_title;
    private String pre_topic;
    private String pre_link;
    private String pre_criteria;
    
    public SCEditerUR(CourseSiteGenerator app, String type, LocalDate date, String time, String title, String topic, String link, String criteria){
        this.app = app;
        this.type = type;
        this.date = date;
        this.time = time;
        this.title = title;
        this.topic = topic;
        this.link = link;
        this.criteria = criteria;
        Schedule temp = app.getData().getScheduleData().getSchedule(date);
        pre_type = temp.getType();
        pre_date = temp.getDate();
        pre_time = temp.getTime();
        pre_title = temp.getTitle();
        pre_topic = temp.getTopic();
        pre_link = temp.getLink();
        pre_criteria = temp.getCriteria();
    }

    @Override
    public void doTransaction() {
        app.getData().getScheduleData().removeSchedule(date);
        app.getData().getScheduleData().addSchedule(type, date, time, title, topic, link, criteria);
    }

    @Override
    public void undoTransaction() {
        app.getData().getScheduleData().removeSchedule(date);
        app.getData().getScheduleData().addSchedule(pre_type, pre_date, pre_time, pre_title, pre_topic, pre_link, pre_criteria);
    }
    
}
