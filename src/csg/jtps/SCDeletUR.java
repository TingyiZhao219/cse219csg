/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Schedule;
import java.time.LocalDate;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class SCDeletUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String type;
    private LocalDate date;
    private String time;
    private String title;
    private String topic;
    private String link;
    private String criteria;
    
    public SCDeletUR(CourseSiteGenerator app, LocalDate date){
        Schedule temp = app.getData().getScheduleData().getSchedule(date);
        type = temp.getType();
        this.date = temp.getDate();
        this.time = temp.getTime();
        title = temp.getTitle();
        topic = temp.getTopic();
        link = temp.getLink();
        criteria = temp.getCriteria();
    }

    @Override
    public void doTransaction() {
        app.getData().getScheduleData().removeSchedule(date);
    }

    @Override
    public void undoTransaction() {
        app.getData().getScheduleData().addSchedule(type, date, time, title, topic, link, criteria);
    }
    
}
