/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class TeamAdderUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String name;
    private Color color;
    private Color textc;
    private String link;
    
    public TeamAdderUR(CourseSiteGenerator app, String name, Color color, Color textc, String link){
        this.app = app;
        this.name = name;
        this.color = color;
        this.textc = textc;
        this.link = link;
    }

    @Override
    public void doTransaction() {
        app.getData().getTeamData().addTeam(name, color, textc, link);
    }

    @Override
    public void undoTransaction() {
        app.getData().getTeamData().removeTeam(name);
    }
    
}
