/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.CsgCourseData;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class CourseTitleUR implements jTPS_Transaction{
    private CourseSiteGenerator app; 
    private String pre_num;
    private String num;
    
    public CourseTitleUR(CourseSiteGenerator app, String num){
        this.app = app;
        this.pre_num = app.getData().getCourseData().getCourse_title();
        this.num = num;
    }

    @Override
    public void doTransaction() {
        CsgCourseData data = app.getData().getCourseData();
        data.setCourse_title(num);
    }

    @Override
    public void undoTransaction() {
        CsgCourseData data = app.getData().getCourseData();
        data.setCourse_title(pre_num);
    }
    
}
