/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.jtps;

import csg.CourseSiteGenerator;
import csg.data.Recitation;
import jtps.jTPS_Transaction;

/**
 *
 * @author zhaotingyi
 */
public class RCDeletUR implements jTPS_Transaction{
    
    private CourseSiteGenerator app;
    private String section;
    private String instructor;
    private String time;
    private String location;
    private String TA1;
    private String TA2;
    
    public RCDeletUR(CourseSiteGenerator app, String section){
        this.app = app;
        this.section = section;
        Recitation temp = app.getData().getRecitationData().getRecitation(section);
        instructor = temp.getInstructor();
        time = temp.getTime();
        location = temp.getLocation();
        TA1 = temp.getTA1();
        TA2 = temp.getTA2();
    }

    @Override
    public void doTransaction() {
        app.getData().getRecitationData().removeRecitation(section);
    }

    @Override
    public void undoTransaction() {
        app.getData().getRecitationData().addRecitation(section, instructor, time, location, TA1, TA2);
    }
    
}
