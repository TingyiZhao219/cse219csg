/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package csg.file;

import csg.CourseSiteGenerator;
import csg.data.CsgCourseData;
import csg.data.CsgData;
import csg.data.CsgRecitationData;
import csg.data.CsgSchedulesData;
import csg.data.CsgStudentData;
import csg.data.CsgTAData;
import csg.data.CsgTeamData;
import csg.data.Recitation;
import csg.data.Schedule;
import csg.data.Student;
import csg.data.TeachingAssistant;
import csg.data.Team;
import csg.workspace.CourseSites;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

/**
 *
 * @author zhaotingyi
 */
public class CsgFile {
    private CourseSiteGenerator app;
    
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_EMAIL = "email";
    
    public CsgFile(CourseSiteGenerator initApp){
        app = initApp;
    }
    
    public void saveData(CsgData data, String filePath) throws IOException {
	// GET THE DATA
//	CsgData dataManager = data;
        CsgTAData TaData = data.getTAData();
        CsgCourseData CourseData = data.getCourseData();
        CsgRecitationData RecitationData = data.getRecitationData();
        CsgSchedulesData ScheduleData = data.getScheduleData();
        CsgTeamData TeamData = data.getTeamData();
        CsgStudentData StudentData = data.getStudentData();

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder untaArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder grtaArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = TaData.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
            if(ta.isSelected())
                untaArrayBuilder.add(taJson);
            else
                grtaArrayBuilder.add(taJson);
                
	}
	JsonArray undergradTAsArray = untaArrayBuilder.build();
	JsonArray gradTAsArray = grtaArrayBuilder.build();

	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(TaData);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + TaData.getStartHour())
		.add(JSON_END_HOUR, "" + TaData.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add("grad_tas", gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
		.build();
	
        JsonObject courseDataJSO = Json.createObjectBuilder()
		.add("Course_num", CourseData.getCourse_num())
		.add("Course_semester", CourseData.getCourse_semester())
		.add("Course_subject", CourseData.getCourse_subject())
		.add("Course_title", CourseData.getCourse_title())
		.add("Course_year", CourseData.getCourse_year())
		.add("Export_dir", CourseData.getExport_dir())
		.add("Instructor_home", CourseData.getInstructor_home())
		.add("Instructor_name", CourseData.getInstructor_name())
		.add("Left_image", CourseData.getLeft_image())
		.add("Right_image", CourseData.getRight_image())
		.add("School_image", CourseData.getSchool_image())
		.add("Site_template", CourseData.getSite_template())
		.build();
        
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
	ObservableList<Recitation> rcs = RecitationData.getRecitations();
	for (Recitation rc : rcs) {	    
	    JsonObject rcJson = Json.createObjectBuilder()
		    .add("Section", rc.getSection())
		    .add("Instructor", rc.getInstructor())
		    .add("Time", rc.getTime())
		    .add("Location", rc.getLocation())
		    .add("TA1", rc.getTA1())
		    .add("TA2", rc.getTA2())
                    .build();
	    recitationArrayBuilder.add(rcJson);
	}
	JsonArray recitaionArray = recitationArrayBuilder.build();
        
        
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
	ObservableList<Schedule> scs = ScheduleData.getSchedules();
	for (Schedule sc : scs) {	    
	    JsonObject scJson = Json.createObjectBuilder()
		    .add("Type", sc.getType())
		    .add("Date", sc.getDate().getDayOfMonth())
		    .add("Month", sc.getDate().getMonthValue())
		    .add("Year", sc.getDate().getYear())
		    .add("Time", sc.getTime())
		    .add("Title", sc.getTitle())
		    .add("Topic", sc.getTopic())
		    .add("Link", sc.getLink())
		    .add("Criteria", sc.getCriteria())
                    .build();
	    scheduleArrayBuilder.add(scJson);
	}
	JsonArray scheduleArray = scheduleArrayBuilder.build();
        JsonObject scheduleDataJSO = Json.createObjectBuilder()
                .add("StartDay", ScheduleData.getStart().getDayOfMonth())
                .add("StartMonth", ScheduleData.getStart().getMonthValue())
                .add("StartYear", ScheduleData.getStart().getYear())
                .add("EndDay", ScheduleData.getEnd().getDayOfMonth())
                .add("EndMonth", ScheduleData.getEnd().getMonthValue())
                .add("EndYear", ScheduleData.getEnd().getYear())
                .add("Schedules", scheduleArray)
                .build();
        
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
	ObservableList<Team> tes = TeamData.getTeams();
	for (Team te : tes) {	    
	    JsonObject teJson = Json.createObjectBuilder()
		    .add("Name", te.getName())
		    .add("ColorR", te.getColor().getRed())
		    .add("ColorG", te.getColor().getGreen())
		    .add("ColorB", te.getColor().getBlue())
		    .add("TextColorR", te.getText_color().getRed())
		    .add("TextColorG", te.getText_color().getGreen())
		    .add("TextColorB", te.getText_color().getBlue())
		    .add("Link", te.getLink())
                    .build();
	    teamArrayBuilder.add(teJson);
	}
	JsonArray teamArray = teamArrayBuilder.build();
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
	ObservableList<Student> sts = StudentData.getStudents();
	for (Student st : sts) {	    
	    JsonObject stJson = Json.createObjectBuilder()
		    .add("FistName", st.getFname())
		    .add("LastName", st.getLname())
		    .add("Team", st.getTeam())
		    .add("Role", st.getRole())
                    .build();
	    studentArrayBuilder.add(stJson);
	}
	JsonArray studentArray = studentArrayBuilder.build();
        
	JsonObject totalData = Json.createObjectBuilder()
                .add("CourseData", courseDataJSO)
                .add("TaData", dataManagerJSO)
                .add("Recitations", recitaionArray)
                .add("Schedules", scheduleDataJSO)
                .add("Teams", teamArray)
                .add("Students", studentArray)
		.build();
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(totalData);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(totalData);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    public void loadData(CsgData data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	CsgData dataManager = data;

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);

	// LOAD THE START AND END HOURS
	String startHour = json.getJsonObject("TaData").getString(JSON_START_HOUR);
        String endHour = json.getJsonObject("TaData").getString(JSON_END_HOUR);
        dataManager.getTAData().initHours(startHour, endHour);

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = json.getJsonObject("TaData").getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.getTAData().addTA(name, email);
            dataManager.getTAData().getTA(name).getUndergrad().selectedProperty().set(true);
        }
        JsonArray jsongrTAArray = json.getJsonObject("TaData").getJsonArray("grad_tas");
        for (int i = 0; i < jsongrTAArray.size(); i++) {
            JsonObject jsonTA = jsongrTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            dataManager.getTAData().addTA(name, email);
            dataManager.getTAData().getTA(name).getUndergrad().selectedProperty().set(false);
        }

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonObject("TaData").getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.getTAData().addOfficeHoursReservation(day, time, name);
        }
        
        //set Course Detail Properties
        JsonObject courseDetail = json.getJsonObject("CourseData");
            dataManager.getCourseData().setCourse_num(courseDetail.getString("Course_num"));
            dataManager.getCourseData().setCourse_semester(courseDetail.getString("Course_semester"));
            dataManager.getCourseData().setCourse_subject(courseDetail.getString("Course_subject"));
            dataManager.getCourseData().setCourse_title(courseDetail.getString("Course_title"));
            dataManager.getCourseData().setCourse_year(courseDetail.getString("Course_year"));
            dataManager.getCourseData().setExport_dir(courseDetail.getString("Export_dir"));
            dataManager.getCourseData().setInstructor_home(courseDetail.getString("Instructor_home"));
            dataManager.getCourseData().setInstructor_name(courseDetail.getString("Instructor_name"));
            dataManager.getCourseData().setLeft_image(courseDetail.getString("Left_image"));
            dataManager.getCourseData().setRight_image(courseDetail.getString("Right_image"));
            dataManager.getCourseData().setSchool_image(courseDetail.getString("School_image"));
            dataManager.getCourseData().setSite_template(courseDetail.getString("Site_template"));
        
        JsonArray jsonRecitationArray = json.getJsonArray("Recitations");
        for (int i = 0; i < jsonRecitationArray.size(); i++) {
            JsonObject jsonRC = jsonRecitationArray.getJsonObject(i);
            String section = jsonRC.getString("Section");
            String Instructor = jsonRC.getString("Instructor");
            String Time = jsonRC.getString("Time");
            String Location = jsonRC.getString("Location");
            String TA1 = jsonRC.getString("TA1");
            String TA2 = jsonRC.getString("TA2");
            dataManager.getRecitationData().addRecitation(section, Instructor, Time, Location, TA1, TA2);
        }
        
        JsonArray jsonScheduleArray = json.getJsonObject("Schedules").getJsonArray("Schedules");
        JsonObject jsonScheduleObject = json.getJsonObject("Schedules");
        {
            int day = jsonScheduleObject.getInt("StartDay");
            int month = jsonScheduleObject.getInt("StartMonth");
            int year = jsonScheduleObject.getInt("StartYear");
            LocalDate date = LocalDate.of(year, month, day);
            dataManager.getScheduleData().setStart(date);
            day = jsonScheduleObject.getInt("EndDay");
            month = jsonScheduleObject.getInt("EndMonth");
            year = jsonScheduleObject.getInt("EndYear");
            LocalDate date2 = LocalDate.of(year, month, day);
            dataManager.getScheduleData().setStart(date2);
        }
        for (int i = 0; i < jsonScheduleArray.size(); i++) {
            JsonObject jsonSC = jsonScheduleArray.getJsonObject(i);
            String type = jsonSC.getString("Type");
            int date = jsonSC.getInt("Date");
            int month = jsonSC.getInt("Month");
            int year = jsonSC.getInt("Year");
            LocalDate datea = LocalDate.of(year, month, date);
            String time = jsonSC.getString("Time");
            String title = jsonSC.getString("Title");
            String topic = jsonSC.getString("Topic");
            String link = jsonSC.getString("Link");
            String criteria = jsonSC.getString("Criteria");
            dataManager.getScheduleData().addSchedule(type, datea, time, title, topic, link, criteria);
        }
        
        JsonArray jsonTeamArray = json.getJsonArray("Teams");
        for (int i = 0; i < jsonTeamArray.size(); i++) {
            JsonObject jsonTE = jsonTeamArray.getJsonObject(i);
            String name = jsonTE.getString("Name");
            Double colorR = jsonTE.getJsonNumber("ColorR").doubleValue();
            Double colorG = jsonTE.getJsonNumber("ColorG").doubleValue();
            Double colorB = jsonTE.getJsonNumber("ColorB").doubleValue();
            Color color = new Color(colorR, colorG, colorB, 1);
            colorR = jsonTE.getJsonNumber("TextColorR").doubleValue();
            colorG = jsonTE.getJsonNumber("TextColorG").doubleValue();
            colorB = jsonTE.getJsonNumber("TextColorB").doubleValue();
            Color textc = new Color(colorR, colorG, colorB, 1);
            String link = jsonTE.getString("Link");
            dataManager.getTeamData().addTeam(name, color, textc, link);
        }
        
        JsonArray jsonStudentArray = json.getJsonArray("Students");
        for (int i = 0; i < jsonStudentArray.size(); i++) {
            JsonObject jsonST = jsonStudentArray.getJsonObject(i);
            String fname = jsonST.getString("FistName");
            String lname = jsonST.getString("LastName");
            String team = jsonST.getString("Team");
            String role = jsonST.getString("Role");
            dataManager.getStudentData().addStudent(fname, lname, team, role);
        }
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
        app.getWorkspace().reloadWorkspace(app.getData());
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    public void exprotWork(String path) throws IOException {
        CsgCourseData CourseData = app.getData().getCourseData();
        CsgTAData TaData = app.getData().getTAData();
        CsgSchedulesData ScheduleData = app.getData().getScheduleData();
        CsgRecitationData RecitationData = app.getData().getRecitationData();
        CsgTeamData TeamData = app.getData().getTeamData();
        CsgStudentData StudentData = app.getData().getStudentData();

        String filePath;
        Map<String, Object> properties;
        JsonWriterFactory writerFactory;
        StringWriter sw;
        JsonWriter jsonWriter;
        OutputStream os;
        JsonWriter jsonFileWriter;
        String prettyPrinted;
        PrintWriter pw;
        
        JsonObjectBuilder homeBuilder = Json.createObjectBuilder();
        homeBuilder.add("Title", CourseData.getCourse_title());
        homeBuilder.add("Subject", CourseData.getCourse_subject());
        homeBuilder.add("Semester", CourseData.getCourse_semester());
        homeBuilder.add("Year", CourseData.getCourse_year());
        homeBuilder.add("Instructor_Name", CourseData.getInstructor_name());
        homeBuilder.add("Instructor_Home", CourseData.getInstructor_home());
        homeBuilder.add("School_image", CourseData.getSchool_image());
        homeBuilder.add("Left_image", CourseData.getLeft_image());
        homeBuilder.add("Right_image", CourseData.getRight_image());
        JsonObject homeJSO = homeBuilder.build();
        
        filePath = System.getProperty("user.dir").replace('\\', '/') + "/public_html/js/Home.js";
        properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        sw = new StringWriter();
        jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(homeJSO);
        jsonWriter.close();
        os = new FileOutputStream(filePath);
        jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(homeJSO);
        prettyPrinted = sw.toString();
        pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
        JsonArrayBuilder scheduleArrayBuilder = Json.createArrayBuilder();
	ObservableList<Schedule> scs = ScheduleData.getSchedules();
	for (Schedule sc : scs) {	    
	    JsonObject scJson = Json.createObjectBuilder()
		    .add("Type", sc.getType())
		    .add("Date", sc.getDate().getDayOfMonth())
		    .add("Month", sc.getDate().getMonthValue())
		    .add("Year", sc.getDate().getYear())
		    .add("Time", sc.getTime())
		    .add("Title", sc.getTitle())
		    .add("Topic", sc.getTopic())
		    .add("Link", sc.getLink())
		    .add("Criteria", sc.getCriteria())
                    .build();
	    scheduleArrayBuilder.add(scJson);
	}
	JsonArray scheduleArray = scheduleArrayBuilder.build();
        JsonObject scheduleDataJSO = Json.createObjectBuilder()
                .add("StartDay", ScheduleData.getStart().getDayOfMonth())
                .add("StartMonth", ScheduleData.getStart().getMonthValue())
                .add("StartYear", ScheduleData.getStart().getYear())
                .add("EndDay", ScheduleData.getEnd().getDayOfMonth())
                .add("EndMonth", ScheduleData.getEnd().getMonthValue())
                .add("EndYear", ScheduleData.getEnd().getYear())
                .add("Schedules", scheduleArray)
                .build();
        filePath = System.getProperty("user.dir").replace('\\', '/') + "/public_html/js/Schedule.js";
        properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        sw = new StringWriter();
        jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(scheduleDataJSO);
        jsonWriter.close();
        os = new FileOutputStream(filePath);
        jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(scheduleDataJSO);
        prettyPrinted = sw.toString();
        pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
	JsonArrayBuilder untaArrayBuilder = Json.createArrayBuilder();
	JsonArrayBuilder grtaArrayBuilder = Json.createArrayBuilder();
	ObservableList<TeachingAssistant> tas = TaData.getTeachingAssistants();
	for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
            if(ta.isSelected())
                untaArrayBuilder.add(taJson);
            else
                grtaArrayBuilder.add(taJson);
                
	}
	JsonArray undergradTAsArray = untaArrayBuilder.build();
	JsonArray gradTAsArray = grtaArrayBuilder.build();
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(TaData);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	JsonArray timeSlotsArray = timeSlotArrayBuilder.build();
        JsonArrayBuilder recitationArrayBuilder = Json.createArrayBuilder();
	ObservableList<Recitation> rcs = RecitationData.getRecitations();
	for (Recitation rc : rcs) {	    
	    JsonObject rcJson = Json.createObjectBuilder()
		    .add("Section", rc.getSection())
		    .add("Instructor", rc.getInstructor())
		    .add("Time", rc.getTime())
		    .add("Location", rc.getLocation())
		    .add("TA1", rc.getTA1())
		    .add("TA2", rc.getTA2())
                    .build();
	    recitationArrayBuilder.add(rcJson);
	}
	JsonArray recitaionArray = recitationArrayBuilder.build();
	JsonObject SyllabusJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + TaData.getStartHour())
		.add(JSON_END_HOUR, "" + TaData.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add("grad_tas", gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add("Recitations", recitaionArray)
		.build();
        filePath = System.getProperty("user.dir").replace('\\', '/') + "/public_html/js/Syllabus.js";
        properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        sw = new StringWriter();
        jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(SyllabusJSO);
        jsonWriter.close();
        os = new FileOutputStream(filePath);
        jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(SyllabusJSO);
        prettyPrinted = sw.toString();
        pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
        
        JsonArrayBuilder teamArrayBuilder = Json.createArrayBuilder();
	ObservableList<Team> tes = TeamData.getTeams();
	for (Team te : tes) {	    
	    JsonObject teJson = Json.createObjectBuilder()
		    .add("Name", te.getName())
		    .add("ColorR", te.getColor().getRed())
		    .add("ColorG", te.getColor().getGreen())
		    .add("ColorB", te.getColor().getBlue())
		    .add("TextColorR", te.getText_color().getRed())
		    .add("TextColorG", te.getText_color().getGreen())
		    .add("TextColorB", te.getText_color().getBlue())
		    .add("Link", te.getLink())
                    .build();
	    teamArrayBuilder.add(teJson);
	}
	JsonArray teamArray = teamArrayBuilder.build();
        
        JsonArrayBuilder studentArrayBuilder = Json.createArrayBuilder();
	ObservableList<Student> sts = StudentData.getStudents();
	for (Student st : sts) {	    
	    JsonObject stJson = Json.createObjectBuilder()
		    .add("FistName", st.getFname())
		    .add("LastName", st.getLname())
		    .add("Team", st.getTeam())
		    .add("Role", st.getRole())
                    .build();
	    studentArrayBuilder.add(stJson);
	}
	JsonArray studentArray = studentArrayBuilder.build();
        
        JsonObject ProjectJSO = Json.createObjectBuilder()
                .add("Team", teamArray)
                .add("Students", studentArray)
                .build();
        
        filePath = System.getProperty("user.dir").replace('\\', '/') + "/public_html/js/Project.js";
        properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        writerFactory = Json.createWriterFactory(properties);
        sw = new StringWriter();
        jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(ProjectJSO);
        jsonWriter.close();
        os = new FileOutputStream(filePath);
        jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(ProjectJSO);
        prettyPrinted = sw.toString();
        pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();
    }
}
