var Course_subject;
var Course_num;
var Course_semester;
var Course_title;
var Course_year;
var Export_dir;
var Instructor_home;
var Instructor_name;
var Left_image;
var Right_image;
var School_image;
var Site_template;

function initCourseDetails() {
    var dataFile = "./js/CourseDetails.json";
    loadData(dataFile, buildCourseDetails);
}

function loadData(jsonFile, callback) {
    $.getJSON(jsonFile, function(json) {
        callback(json);
    });
}

function buildCourseDetails(json) {
	Course_subject = json.Course_subject;
	Course_num = json.Course_num;
	Course_semester = json.Course_semester;
	Course_title = json.Course_title;
	Course_year = json.Course_year;
	Export_dir = json.Export_dir;
	Instructor_home = json.Instructor_home;
	Instructor_name = json.Instructor_name;
	Left_image = json.Left_image;
	Right_image = json.Right_image;
	School_image = json.School_image;
	Site_template = json.Site_template;
    var banner = $("#banner");
    banner.append(Course_subject + ' ' + Course_num + ' - ' + Course_semester + ' ' + Course_year + '<br>' + Course_title);
}